package main

import (
	"flag"
	"fmt"

	"bitbucket.org/tiagoharris/fibonacci/fibo"
)

func main() {
	var n uint64
	flag.Uint64Var(&n, "n", 0, "n")
	flag.Parse()

	fmt.Printf("Fibonacci number calculation for %d: %d\n", n, fibo.SequentialFibonacciBig(uint(n)))
}
