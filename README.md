# fibonacci

Sample golang project to show how to easily write benchmark tests.

The main article can be found [here](https://www.linkedin.com/pulse/golang-benchmarking-made-easy-tiago-melo/).

### running it

```
make run N=5
```

### testing it

```
make test
```

## running benchmark tests

```
make benchmark
```

### available targets in [Makefile](Makefile)

```
make help
```